import React, { Component } from 'react';
import {
  BrowserRouter,
  Link,
  Route,
  Switch
} from 'react-router-dom';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import NotFound from './pages/NotFound';
import SinglePost from './pages/SinglePost';
import Post from './pages/Posts';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        {/* menu */}
        <ul>
          <Link to='/'><li>Home</li></Link>
          <Link to='/about'><li>About</li></Link>
          <Link to='/contact'><li>Contact</li></Link>
          <Link to='/posts'><li>posts</li></Link>
          <Link to='/post/1234'><li>Post</li></Link>
        </ul>

        {/* pages */}
        <Switch>

          {/* <Route exact path="/">
            <Home />
          </Route> */}
          <Route path='/' exact component={Home} />

          <Route path="/about" component={About} />

          <Route path="/contact" component={Contact} />

          <Route path="/posts" component={Post} />

          <Route path="/post/:postId" component={SinglePost} />

          <Route path="*" component={NotFound} />

        </Switch>

        {/* footer */}
      </BrowserRouter>
    );
  }
}

export default App;
