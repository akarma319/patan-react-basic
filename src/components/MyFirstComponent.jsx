import React, { Component } from 'react'

class MyFirstComponent extends Component {
    render() {
        console.log('here', this.props);
        let details = this.props.myDetails;
        console.log(details);
        let filterData = [];

        details.forEach((data, i) => {
            if (filterData.length < 2) {
                filterData.push(data);
            }
        });
        return (
            <div>
                {
                    filterData.map((data, i) => {
                        return (
                            < div className="container" key={i}>
                                <h1>Name: {data.name}</h1>
                                <h1>Address: {data.location}</h1>
                                <h1>{this.props.myMessage}</h1>
                            </div >
                        );
                    })
                }
            </div>

        );
    }
}

export default MyFirstComponent
