import React, { Component } from 'react'

class FormsExample extends Component {
    constructor() {
        super();
        this.state = {
            fullName: '',
            message: ''
        }
    }

    handleChange = (event) => {
        // console.log(event);
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        // api request
        if (this.state.fullName === '') {
            return alert('name is required.');
        }
        alert('Submited!')
    }

    render() {
        console.log(this.state.message);
        return (
            <div className='container'>
                <h1>Forms Example</h1>

                <form onSubmit={this.handleSubmit}>
                    <input
                        type='text'
                        value={this.state.name}
                        onChange={this.handleChange}
                        name="fullName"
                    />
                    <input
                        type='text'
                        value={this.state.message}
                        onChange={this.handleChange}
                        name="message"
                    />
                    <p>Name: {this.state.fullName}</p>
                    <p>message: {this.state.message}</p>
                    <input type='submit' value='submit'></input>
                </form>
            </div>
        )
    }
}

export default FormsExample
