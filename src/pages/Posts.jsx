import React, { Component } from 'react';
import axios from 'axios';

export class Post extends Component {
    constructor() {
        super();
        this.state = {
            posts: []
        }
    }

    async componentDidMount() {
        //api call
        try {
            let { data: posts } = await axios.get('http://localhost:5000/api/');

            this.setState({
                posts
            });
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        console.log('my state', this.state.posts);
        return (
            <div>
                <h1>All Post</h1>
                <ol>
                    {
                        this.state.posts.map(post => {
                            return <li key={post._id}>{post.title}({post.createdAt})</li>
                        })
                    }

                </ol>
            </div>
        )
    }
}

export default Post
