import React, { Component } from 'react'
import FormsExample from '../components/FormsExample'

export class NotFound extends Component {
    render() {
        return (
            <h1>404 Not Found</h1>
        )
    }
}

export default NotFound
