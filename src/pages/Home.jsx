import React, { Component } from 'react'
import MyFirstComponent from '../components/MyFirstComponent'
import FormsExample from '../components/FormsExample'

export class Home extends Component {
    constructor() {
        super();
        this.state = {
            details: [
                {
                    name: 'Aabhas Karmacharya',
                    location: 'Kathmandu'
                },
                {
                    name: 'Ram',
                    location: 'Lalitpur'
                },
                {
                    name: 'Hari',
                    location: 'Bhaktapur'
                }
            ],
            count: 0,
            light: false
        }

        // this.myOnClickhandler = this.myOnClickhandler.bind(this);
    }

    componentWillMount() {
        console.log('Hello from will mount');
    };

    componentDidMount() {
        // api fetch
        console.log('Hello from did mount');
    };

    myOnClickhandler = () => {
        // console.log('click!', this.state);
        this.setState({
            count: this.state.count + 1
        });
    }

    myLightHandler = () => {
        this.setState({
            light: !this.state.light
        });
    }

    render() {
        let lightFlag = '';
        if (this.state.light) {
            lightFlag = 'Light ON'
        } else {
            lightFlag = 'Light OFF'
        }

        // let details = this.state.details;
        let message = 'Namaste';
        return (
            <div>
                <h1 style={headerStyle}>Hello world!(MENU)</h1>

                <MyFirstComponent myDetails={this.state.details} myMessage={message} />

                <h1>{this.state.count}</h1>
                <button onClick={this.myOnClickhandler}>Click me</button>

                <h1>{this.state.light ? 'Light ON' : 'Light OFF'}</h1>
                <h1>{lightFlag}</h1>
                <button onClick={this.myLightHandler}>{this.state.light ? 'OFF' : 'ON'}</button>

                <FormsExample />

            </div>
        )
    }
}

let headerStyle = {
    color: 'red',
    textAlign: 'center'
}

export default Home
